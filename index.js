var express = require('express');
var cors = require('cors');
var app = express();

if(process.env.NODE_ENV && process.env.NODE_ENV === 'production') {
  // In production we just want to run the dist package as a static app.
  app.use(express.static(__dirname + '/dist'));
} else if(process.env.NODE_ENV && process.env.NODE_ENV === 'development') {
  // In development we really want to update this stuff in realtime.
  app.use('/bower_components', express.static('./bower_components'));
  app.use(express.static(__dirname + '/.tmp') );
  app.use(express.static(__dirname + '/app'));
}

app.use(cors());

app.set('port', (process.env.PORT || 5000));

app.listen(app.get('port'), function() {
  console.log("Hello, Your node app is running at localhost:" + app.get('port'));
});
