(function (window, angular, undefined) {
  'use strict';
  angular
    .module('app.player')
    .controller('PlayerCtrl', ['$scope', function($scope) {

      var vm = this;
      vm.playerLoaded = false;
      vm.player = null;

      vm.onPlayerLoaded = function() {
        // now you can see the player for future dev/reference;
        vm.playerLoaded = true;
        vm.player = videojs.players['videoPlayer'];
        // since this is triggered outside of Angular via onload you need to run a digest cycle.
        $scope.$apply();
        // Let the world know your player is loaded
        $scope.$emit('Video:Player:Loaded', vm.player);
      };

      vm.loadPlayerScript = function() {
        if(vm.playerId && vm.accountId) {
         vm.scriptUrl = 'http://players.brightcove.net/' + vm.accountId + '/' + vm.playerId +'_default/index.js';
        }
      };

      $scope.$watch('playerId', function(newValue) {
        vm.playerId = newValue;
        vm.loadPlayerScript();
      });

      $scope.$watch('accountId', function(newValue) {
        vm.accountId = newValue;
        vm.loadPlayerScript();
      });

      $scope.$watch('videoId', function(newValue) {
        if(newValue) {
          vm.videoId = newValue;
        }
      });

      // External Requests to Change Video Reference Id / Source
      $scope.$on('Video:Src:Load', function(event, src) {
        if(vm.playerLoaded && vm.player) {
          console.log('load new src', src);
          vm.player.catalog.getVideo('ref:'+src, function(err,info){
            if(!err && info) {
              vm.player.catalog.load(info);
            }
            //TODO: What should we do if no new video info is returned?
          });
        }
      });

      $scope.$on('Video:Script:Load:Complete', function() {
        vm.onPlayerLoaded();
      });

    }]);
})(window, window.angular);
