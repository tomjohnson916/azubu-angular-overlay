(function (window, document, angular, undefined) {
  'use strict';

  angular
    .module('app.player')
    .directive('azubuVideoScript', [function() {
      return function(scope, element, attrs) {
        var s = document.createElement('script');
            s.src = attrs.scriptUrl;
            s.onload = function() {
              scope.$emit('Video:Script:Load:Complete');
            };
        element[0].appendChild(s);
      };
    }])

    .directive('azubuVideoPlayer', [function() {
      return {
        restrict: 'EA',
        scope: {
          'accountId': '@',
          'videoId': '@',
          'playerId': '@',
          'posterUrl': '@'
        },
        templateUrl: 'app/player/module/directive/azubu-video-player.html',
        controller: 'PlayerCtrl as player'
      };
    }]);

})(window, window.document, window.angular);
