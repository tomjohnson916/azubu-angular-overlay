(function (window, angular, undefined) {
  'use strict';

  angular.module('app.player', []);

  angular
    .module('app.player')
    .value ('videojs', window.videojs);

})(window, window.angular);
