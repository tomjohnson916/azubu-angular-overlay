'use strict';

angular.module('azubuOverlay')
  .controller('MainCtrl', ['$scope', '$window', function ($scope, $window) {
    var skOverlay = {
      name: 'SK Sports Logo',
      url: 'http://www.sksports.net/IMG/Global/main/title01_main.png',
      href: '//www.sksports.net/',
      rect: {
        x: 800,
        y: 80,
        width: 96,
        height: 67
      },
      container: {
        width: 960,
        height: 540
      }
    };

    var nbOverlay = {
      name: 'New Balance Sponsor',
      url: 'http://beta.azubu.tv/images/bricks_assets/55720f69441357193b438786/sponsor_newbalancekorea__1433544552.png',
      href: '//www.newbalance.com/',
      rect: {
        x: 30,
        y: 55,
        width: 171,
        height: 87
      },
      container: {
        width: 960,
        height: 540
      }
    };

    var corOverlay = {
      name: 'Corsair Sponsor',
      url: 'http://beta.azubu.tv/images/bricks_assets/55720f69441357193b438786/sponsor_corsair__1433544673.png',
      href: '//www.corsair.com/en/',
      rect: {
        x: 760,
        y: 90,
        width: 167,
        height: 42
      },
      container: {
        width: 960,
        height: 540
      }
    };

    var comboOverlay = {name: 'SK Template', items: [skOverlay, nbOverlay]};

    $scope.MAIN_PLAYER_DOM_ID = 'videoPlayer';
    $scope.OVERLAY_URL = 'http://players.brightcove.net/3361910549001/videojs-azubu-overlay/src/videojs-azubu-overlay.js'; //'/bower_components/videojs-azubu-overlay/src/videojs-azubu-overlay.js';
    $scope.OVERLAY_DEFAULTS = {
      name: 'Default Overlay',
      url: '//azubu.tv/favicon.ico',
      href: '//www.azubu.tv/',
      rect: {
        x: 0,
        y: 0,
        width: 128,
        height: 128
      },
      container: {
        width: 960,
        height: 540
      }
    };
    //http://beta.azubu.tv/images/bricks_assets/55720f69441357193b438786/sponsor_corsair__1433544673.png
    // 334, 85

    //http://beta.azubu.tv/images/bricks_assets/55720f69441357193b438786/sponsor_newbalancekorea__1433544552.png
    // 370, 222

    //http://www.sksports.net/IMG/Global/main/title01_main.png
    // 343, 175 skports.net

    $scope.loadedTemplates = [skOverlay, nbOverlay, corOverlay, comboOverlay];

    $scope.editMode = false;
    $scope.loadedOverlays = [];
    $scope.currentOverlay = angular.copy($scope.OVERLAY_DEFAULTS);
    $scope.playerSize = 540;
    $scope.videoJSReady = false;
    $scope.player = null;
    $scope.playerDimensions = {
      width: 0,
      height: 0
    };

    // HLS DEMO PROD : f5a518ab-f47c-4544-97f9-8fa98539bbda
    // PREV : 3f1af37e-749a-48cc-b1d4-6465851b3bfc

    $scope.broadcastInfo = {
      playerId: '3f1af37e-749a-48cc-b1d4-6465851b3bfc',
      accountId: '3361910549001',
      videoId: 'video36717SVgorgntv',
      posterUrl: 'http://azubuhd.brightcove.com.edgesuite.net/pd/2965022465001/201505/2965022465001_4247165864001_channel.jpg?pubId=2965022465001'
    };

    $scope.addTemplate = function(overlay) {
      if(overlay.items) {
        angular.forEach(overlay.items, function(item) {
          $scope.currentOverlay = item;
          $scope.createOverlay();
          $scope.saveOverlay();
        })
      } else {
        $scope.currentOverlay = overlay;
        $scope.createOverlay();
        $scope.editOverlay(overlay);
      }
    };

    $scope.setupPlayer = function() {
      $scope.videoJSReady = ($window.videojs.players !== undefined);
      $scope.player = $window.videojs.players[$scope.MAIN_PLAYER_DOM_ID];
      $scope.playerDimensions.width = $scope.player.width();
      $scope.playerDimensions.height = $scope.player.height();
      $scope.player.azubuOverlay();
    };

    $scope.onOverlayPluginLoaded = function() {
      $scope.setupPlayer();
      $scope.$apply();
    };

    $scope.loadOverlayScript = function() {
      $scope.loadScriptUrl($scope.OVERLAY_URL, $scope.onOverlayPluginLoaded);
    };

    $scope.loadScriptUrl = function(url, cb) {
      var existsInDOM = false;

      angular.forEach($window.document.getElementsByTagName('script'), function(script) {
        if(script.src && script.src === url) {
          existsInDOM = true;
        }
      });

      if(!existsInDOM) {
        var s = $window.document.createElement('script');
        s.src = url;
        s.onload = function(){
          if(cb) {
            cb();
          }
        };
        $window.document.body.appendChild(s);
      }
    };

    $scope.$watch('sizeModel', function(newVal, oldVal) {
      if(newVal && newVal !== oldVal) {
        $scope.setOverlayDimensions(newVal.split('x')[0],newVal.split('x')[1]);
      }
    });

    $scope.setOverlayDimensions = function(width, height) {
      $scope.currentOverlay.rect.width = width;
      $scope.currentOverlay.rect.height = height;
    };

    $scope.elementCounter = 900;

    $scope.addOverlay = function() {
      console.log('add overlay', $scope.currentOverlay);

      var overlay = $scope.player.addOverlay($scope.currentOverlay);
      overlay.el().className += ' vjs-azubu-overlay-display';
      overlay.show();

      $scope.loadedOverlays.push({
        config: $scope.currentOverlay,
        el: overlay
      });
    };

    $scope.currentOverlayID = null;

    $scope.onValuesChanged =  function(newVal, oldVal) {
      if($scope.editMode && newVal && oldVal && newVal !== oldVal) {
        $scope.player.setOverlayPositionById($scope.currentOverlayID, $scope.currentOverlay.rect.x, $scope.currentOverlay.rect.y);
        $scope.player.setOverlayDimensionsById($scope.currentOverlayID, $scope.currentOverlay.rect.width, $scope.currentOverlay.rect.height);
        $scope.player.setOverlayNameById($scope.currentOverlayID, $scope.currentOverlay.name);
      }
    };

    $scope.editOverlay = function(value) {
      console.log('edit', value);
      var overlay = $scope.player.AZUBU.overlays[value.id];

      $scope.currentOverlay = overlay.config;
      $scope.currentOverlayID = overlay.id;
      overlay.el().className += ' vjs-azubu-overlay-display';
      $scope.editMode = true;

    };

    $scope.createOverlay = function() {
      $scope.editMode = true;
      var overlay = $scope.player.addOverlay($scope.currentOverlay);
          overlay.el().className += ' vjs-azubu-overlay-display';
          overlay.show();

      $scope.currentOverlayID = overlay.id;
    };

    $scope.saveOverlay = function() {
      $scope.editMode = false;

      var overlay = $window.document.querySelector('.vjs-azubu-overlay-display');

      if(overlay) {
        overlay.className -= ' vjs-azubu-overlay-display';
      }

      $scope.loadedOverlays = [];
      for(var i in $scope.player.AZUBU.overlays) {
        $scope.loadedOverlays.push({
          name: $scope.player.AZUBU.overlays[i].name,
          id: $scope.player.AZUBU.overlays[i].id
        });
      }

      $scope.currentOverlay = angular.copy($scope.OVERLAY_DEFAULTS);
    };

    $scope.removeOverlay = function() {
      $scope.player.removeOverlay($scope.currentOverlayID);
      $scope.currentOverlay = angular.copy($scope.OVERLAY_DEFAULTS);
      $scope.editMode = false;
      $scope.loadedOverlays = [];
      for(var i in $scope.player.AZUBU.overlays) {
        $scope.loadedOverlays.push({
          name: $scope.player.AZUBU.overlays[i].name,
          id: $scope.player.AZUBU.overlays[i].id
        });
      }
    };

    $scope.moveOverlay = function() {
      // go get the overlay
      // move it
      // save
      $scope.player.setOverlayPositionById($scope.currentOverlayID, $scope.currentOverlay.rect.x, $scope.currentOverlay.rect.y);
    };

    $scope.resizePlayer = function(width, height) {
      $scope.player.width(width);
      $scope.player.height(height);
    };

    $scope.onSizeChanged = function(newVal,oldVal) {
      if(newVal && oldVal && newVal !== oldVal) {
          var playerW, playerH = newVal;
          switch (newVal) {
            case 360:
              playerW = 640;
              break;

            case 540:
              playerW = 960;
              break;

            case 720:
              playerW = 1280;
              break;

            case 1080:
              playerW = 1920;
              break;

            default:
              break;
          }
        $scope.resizePlayer(playerW, playerH);
      }
    };

    $scope.$watch('currentOverlay.name', $scope.onValuesChanged);
    $scope.$watch('currentOverlay.rect.x', $scope.onValuesChanged);
    $scope.$watch('currentOverlay.rect.y', $scope.onValuesChanged);
    $scope.$watch('currentOverlay.rect.width', $scope.onValuesChanged);
    $scope.$watch('currentOverlay.rect.height', $scope.onValuesChanged);
    $scope.$watch('playerSize', $scope.onSizeChanged);

    //$scope.$on('Video:Script:Load:Complete', $scope.onOverlayPluginLoaded);
    $scope.$on('Video:Script:Load:Complete', $scope.loadOverlayScript);

  }]);
