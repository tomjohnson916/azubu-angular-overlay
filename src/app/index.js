'use strict';

angular.module('azubuOverlay', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'restangular', 'ui.router', 'ui.bootstrap' ,'app.player'])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      });

    $locationProvider.html5Mode({enabled: true, requireBase: false});

    $urlRouterProvider.otherwise('/');
  })
;
